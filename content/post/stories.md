---
title: Stories
subtitle: Our students' success stories
date: 2020-06-17

---
### List of colleges we sent our students

***

#### USA

University of Alabama @ Birmingham, AL

University of Arkansas @ Little Rock, AR

Arizona State University @ Tempe, AZ

University of California @ Berkeley, CA

University of Southern California @ Los Angeles, CA

University of California @ Irvine, CA

University of California @ San Diego,CA

University of Colorado @ Boulder, CO

Colorado State University @ Fort Collins, CO

University of Denver, Denver, CO

Florida International University, Tamiami Trail, FL

Florida State University, Tallahassee, FL

Florida Atlantic University, Boca Raton, FL

University of Florida, Gainesville, FL

University of South Florida

University of Central Florida

University of Miami

University of Connecticut @ Storrs, CT

University of Georgia @ Athens, GA

Georgia Institute of Technology, Atlanta, GA

University of Illinois @ Urbana Champaign, IL

Southern Illinois University at Carbondale, IL

Northwestern University, Chicago, IL

Illinois Institute of Technology, Chicago, IL

Rose Hulman Institute of Technology, Terre Haute, IN

Indiana University @ Bloomington, IN

Ball State University, Muncie, IN

University of Idaho, Pocatello, ID

Idaho State University, Boise, ID

Iowa State University, Ames, IA

Kansas State University, Manhattan, KS

University of Kansas, Lawrence, KS

University of Kentucky, Lexington, KY

University of Louisville, Louisville, KY

Louisiana State University, Baton Rouge, LA
University of Louisiana @ Lafayette, LA

University of Maryland, College Park, MD

University of Massachusetts @ Amherst, MA

Tufts University, Medford, MA

Boston College, Boston, MA

University of Michigan, Ann Arbor, MI

Wayne State University, Detroit, MI

Western Michigan University, Kalamazoo, MI

Michigan Technological University, Houghton, MI

University of Detroit, Detroit, MI

Lawrence Technological University, Southfield, MI

Eastern Michigan University, Ypsilanti, MI

Central Michigan University, Mount Pleasant, MI

University of Minnesota @ Minneapolis, MN

Milsaps College, Jackson, MS

Mississippi State University, MS

Washington University, St. Louis, MO

University of Missouri @ Rolla, MO

University of Missouri @ Kansas City, MO

University of Montana, Missoula, MT

Montana State University @ Bozeman, MT

University of Nebraska at Lincoln
University of Wisconsin at Madison
Marquette University

University of Nevada @ Las Vegas, NV

University of North Dakota @ Grand Forks, ND

University of North Carolina @ Chapel Hill, NC
North Carolina State University @ Raleigh

University of New Hampshire @ Concord, NH


Dartmouth College, Hanover, NH


Cornell University, Ithaca, NY


Columbia University, New York, NY


New York University, New York, NY


City University of New York, New York, NY


State University of New York @ Albany, NY


State University of New York @ Buffalo, NY


State University of New York @ Binghamton, NY

Willamette University, Salem, OR
Oregon State University, Corvallis, OR

University of Akron, Akron, OH
University of Dayton, Dayton, OH
University of Cincinnati, Cincinnati, OH
Wright State University, Dayton, OH
Ohio State University @ Columbus, OH
Bowling Green State University, Bowling Green, OH

Pennsylvania State University, University Park, PA
Drexel University, Philadelphia, PA
University of Pittsburgh, Pittsburgh, PA
Carnegie Mellon University, Pittsburgh, PA

Clemson University, Clemson, SC
South Carolina State University, Orangeburg, SC
University of South Carolina, Columbia, SC

South Dakota School of Mines, Rapid City, SD

University of Tennessee @ Knoxville, TN
University of Tennessee @ Chattanooga
Tennessee Technological University, Cookeville, TN
Vanderbilt University, Nashville, TN

The University of Texas @ Austin, TX
The University of Texas @ Dallas, TX
The University of Texas @ Arlington, TX
The University of Texas @ San Antonio, TX
Texas A&M, College Station, TX
Texas Tech University, Lubbock, TX
Baylor University, Waco, TX
University of North Texas, Denton, TX

Utah State University, Logan, UT
Brigham Young University, Provo, UT
University of Utah @ Salt Lake City, UT
University of Virginia @ Charlottesville, VA
University of Richmond, Richmond, VA
Virginia Tech, Blacksburg, VA

West Virginia University, Morgantown, WV

University of Wyoming @ Laramie

#### United Kingdom

University of Aberdeen, Aberdeen, UK

University of Leeds, Leeds, UK

University of Manchester, Manchester City, UK

University of Birmingham, Birmingham, UK

University of Bolton, Bolton, UK

#### Australia

Flinders University, Bedford Park, AU

Monash University, Clayton, Victoria, AU

University of Canberra, Canberra, AU

University of Wollongong, NSW, AU

#### Canada

University of Alberta, Edmonton, CA

University of Calgary, Calgary, CA

University of British Columbia, Vancouver, CA

University of Manitoba, Winnipeg, CA

Dalhousie University, Halifax, CA

McMaster University, Hamilton, CA

University of Toronto, Toronto, CA

#### Singapore

National University of Singapore, Singapore

#### New Zealand

Lincoln University, Christchurch, NZ

Massey University, Wellington, NZ

University of Otago, Dunedin, NZ

University of Auckland, Auckland, NZ