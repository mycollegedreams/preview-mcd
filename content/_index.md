+++

+++
### 'You Dream'. We Help Make It Happen

Welcome. At MyCollegeDreams, we continuously strive to make your college dreams come true.

We truly understand and recognize how stressful and demanding the college application process is for both the students and the parents. There are a multitude of major stresses for college bound students: academic, personal, family, financial and future. 

Our services are customized for every student in order to strengthen their candidacy at the college of their choice and are streamlined to make the journey into college 'stress free'. 

We help you navigate the labyrinth of college application process by working with both the student and the family. We identify the resources required for scoring high in the SAT/ACT, provide guidance in the financial aid applications, personal essays and a short list of colleges where the student has best chance of admission with some financial aid. 

Our concierge programs help students not only get into colleges but also guarantee some percentage of tuition scholarships. 