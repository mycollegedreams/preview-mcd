---
title: About Us
subtitle: About My College Dreams
comments: false

---
### You Dream. We Make It Happen

College bound students and their families face a multitude of stresses: academic, personal, family, financial and future. Identifying the right colleges, understanding the admission requirements, the application process itself, finding adequate finances for college are all daunting and demanding tasks.

At MyCollegeDreams, our only goal is to make your college dreams come true. We use a highly selective process to choose students we want to represent because our measure of success is wholly dependent on not just meeting but exceeding your needs and expectations.

Our dedicated and exceptional services are customized to suit your individual needs. Given how important and critical the final outcomes are to the students and their families, we select very few students every academic year to work with. This has over the years allowed us to continually meet our commitment to surpass your expectations.

At MyCollegeDreams, we strongly believe that our vast experience and proven expertise in the college admission process, financial aid, scholarships, visa requirements, visa interview preparation, and relocation assistance (for overseas applicants) are inimitable values to the services we offer.

There are many college counselling centers that offer similar services, but our forte has been that we not only help you get an admission into colleges but in most cases some sort of tuition assistance and scholarships. These scholarships substantially reduce the cost of college attendance, helping both the student and their family.

We help navigate the labyrinth of college application process by working with both the student and their families by identifying the resources required for scoring high in the SAT/ACT, providing guidance in the college and financial aid applications, mentoring on how to build candidate profiles that stand out, compelling personal essays and a ‘short list of colleges’ where the student has the best chance of admission and financial aid.

Our team is made up of dedicated and passionate professionals with proven expertise and commitment to helping aspiring students. We take pride in understanding the detailed needs of every student and design customized road-maps to achieve the defined results. 

Your journey with us starts with an initial comprehensive and thorough evaluation of the student’s credentials, interests, and academic goals. This endeavor will help us determine if our services are the right fit to your credentials and needs. 

To find out more, kindly provide as much details as you can in the attached ‘Student Self-Assessment’ form and email it back to collegedreams@yahoo.com by the August 31st deadline.

For further questions, please <a href="/page/contact/">contact us</a> today to learn on how we can solidify your student’s college and eventual career dreams.